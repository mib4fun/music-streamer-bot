'use strict'

const Service = require('trails/service')
const mpd = require('mpd'),
      cmd = mpd.cmd
const PORT = 6602

/**
 * @module PlayerService
 * @description Player
 */

module.exports = class PlayerService extends Service {

   connect(host) {
     let  client = require('node-spop/client');
     return client.connect({
          host: host,
          port: PORT
      })
      .then(() => {
        return client
      })
   }

   sendCommand(client, command) {
      return new Promise((res, rej) => {
       client.exec(command)
       .then(res, rej)
      })
      .then(() => {
        client.end()
      })
   }

   search(host, search) {
     return this.connect(host)
       .then((client) => {
          return this.sendCommand(client, `search "${search}"`)
        });
   }

   add(host, trackid) {
       return this.connect(host)
       .then((client) => {
          return this.sendCommand(client, `add "${trackid}"`)
        });
   }

   play(host, uri) {
      return this.connect(host)
       .then((client) => {
          return this.sendCommand(client, `uplay ${uri}`)
        });
   }

   autorepeat(host) {
       return this.connect(host)
       .then((client) => {
          return this.sendCommand(client, `repeat`)
        });
   }
}

