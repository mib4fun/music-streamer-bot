'use strict'

const Service = require('trails/service')
const HyperApi = require('hyper-api')
const Hyper = new HyperApi({
  credential: {
    accessKey: 'VAPOVEEVO8RR14PZXNO4GZZ5',
    secretKey: 'EkGnm9W3doYs33WRD1kZQCAmB6VBLqrsDb36ZbNr'
  }
})
const DOCKER_IMAGE="mib4fun/spopd-spotify:latest"

/**
 * @module BotService
 * @description Create bot
 */
module.exports = class BotService extends Service {

  async getBots() {
    return Hyper.get('/containers/json')
  }

  async create(username, password) {

    const hostname = "spotify-" + username.split('@')[0].replace('.', '-');
    console.log(hostname)
    return Hyper.post('/containers/create', {
      hostname: hostname,
      "Image": DOCKER_IMAGE,
      // "Cmd": '/usr/bin/mopidy',
      "Env": [
        `USERNAME=${username}`,
        `PASSWORD=${password}`,
        // `CLIENT_ID=${client_id}`,
        // `CLIENT_SECRET=${client_secret}`,
      ],
      "Labels": {
          "hostname": hostname,
          "username": username,
          "type": "spotify",
          "sh_hyper_instancetype": "s1"
       },
       "HostConfig": {
       }
    })
    .then(c => {
      console.log(c)
      return Hyper.post(`/containers/${c.Id}/start`)
          .catch(console.log)
    })

    // let createOptions = {
    //   Image: 'ubuntu',
    //   Cmd: 'date'
    // }
    // Hyper.post('/containers/create', createOptions)
    //      .then(c => console.log(c))
    //       .catch((e) => console.log(e))
  }

  async searchSong(search) {
    // const bots = await this.getBots()
    // const host = bots[0].Labels.hostname
    const host = 'localhost';
    let song = await this.app.services.PlayerService.search(host, 'just dormir')
    return song
  }

  async play(uri) {
    const bots = await this.getBots()
    bots
    .filter(bot => bot.Image == DOCKER_IMAGE)
    .map(bot => {
      let host =  bot.Labels.hostname
      this.app.log.info('play to ' + host)
      this.app.services.PlayerService.play(host, uri)
    })
  }

  async launchSong(track) {

  }

  async searchTrack(track) {

  }
}

