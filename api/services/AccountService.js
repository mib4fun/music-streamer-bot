'use strict'

const Service = require('trails/service')
const got = require('got')
const ACCOUNTS = "https://music-streamer-bot.firebaseio.com/accounts.json"
/**
 * @module AccountService
 * @description Create accoount
 */
module.exports = class AccountService extends Service {

  async getAll() {
    return got(ACCOUNTS)
  }
}

