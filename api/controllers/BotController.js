'use strict'

const Controller = require('trails/controller')

/**
 * @module BotController
 * @description TODO document Controller.
 */
module.exports = class BotController extends Controller {

  /*
  * @GET('/bot')
  */
  createBot(req, res) {

  }

  /*
  * @GET('/search')
  */
  searchSong(req, res) {
    this.app.services.BotService.searchSong(req.query.q)
    .then(res.json)
  }

  /*
  * @POST('/play')
  */
  playSong(req, res) {
    this.app.services.BotService.playSong(req.query.q)
    .then(res.json)
  }
}

