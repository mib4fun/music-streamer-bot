'use strict'

const Controller = require('trails/controller')

/**
 * @module ManagerController
 * @description TODO document Controller.
 */
module.exports = class ManagerController extends Controller {
  /*
  * @GET('/bot')
  */
  createBot(req, res) {

  }

  /**
  * @GET('/search')
  */
  searchSong(req, res) {
    this.app.services.ManagerService.searchSong(req.query.q)
    .then(res.json)
  }

  /**
   * @POST('/play')
   */
  playSong(req, res) {
    this.app.services.ManagerService.play(req.body.uri)
    .then(res.json)
  }
}

