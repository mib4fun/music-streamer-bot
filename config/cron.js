/**
 * Cron Configuration
 * (app.config.cron)
 *
 * Configure cron tasks
 *
 * @see {@link https://github.com/jaumard/trailpack-cron}
 */
module.exports = {
  defaultTimeZone: 'Africa/Abidjan', // Default timezone use for tasks
  jobs: {
    myJob: {
      schedule: '*/50 * * * * *',
      onTick: app => {
        console.log('play')
        app.services.ManagerService
        .play()
      },
      onComplete: app => {
        app.log.info('I am done')
      },
      start: true, // Start task immediately
    }
  }
}
